public class JavaVariables6 {
    public static void main(String[] args) {
        int myNum = 5;
        System.out.println(myNum);
        float myFloatNum = 5.99f;
        System.out.println(myFloatNum);
        char myLetter = 'D';
        System.out.println(myLetter);
        boolean myBool = true;
        System.out.println(myBool);
        String myText = "Hello";
        System.out.println(myText);
    }

}
